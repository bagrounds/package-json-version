#!/usr/bin/env node
/**
 *
 * @module package-json-version
 */
;(function () {
  'use strict'

  /* imports */
  var path = require('path')

  var packageJsonPath = process.argv[2] || path.join(__dirname, 'package.json')

  var packageJson = require(packageJsonPath)

  console.log(packageJson.version)
})()

